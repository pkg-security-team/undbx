undbx (0.21-4) unstable; urgency=medium

  * Team upload.
  * debian/control:
      - Bumped Standards-Version to 4.6.1.
      - Migrated DH level to 13.
  * debian/copyright:
      - Converted the last paragraph of the GPL-3 in a comment.
      - Updated packaging copyright years.
  * debian/patch/fix-makefile:
      - Renamed to 010_fix-makefile.patch.
      - Refreshed.
  * debian/rules:
      - Removed no longer needed addon '--with autoreconf' because it is
        default since DH 10.
      - Replaced override_dh_auto_install by execute_after_dh_auto_install.
  * debian/tests/control: set all tests as superficial.
  * debian/watch:
      - Bumped to version 4.
      - Updated the search rule to make it compliant with new standards of
        the GitHub.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 26 Dec 2022 19:55:02 -0300

undbx (0.21-3) unstable; urgency=medium

  * Team upload.

  [ Joao Eriberto Mota Filho ]
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Removed empty Uploaders field.
  * debian/copyright:
      - Added packaging rights for Raphaël Hertzog and Samuel Henrique.
      - Updated packaging copyright years.
  * debian/tests/control: created to perform trivial CI tests.
  * debian/upstream/metadata: added new fields: Repository, Repository-Browse,
    Bug-Database and Bug-Submit.

  [ Raphaël Hertzog ]
  * Drop Christophe Monniez from Uploaders.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 14 Apr 2020 13:55:40 -0300

undbx (0.21-2) unstable; urgency=medium

  * Team upload.
  [ Raphaël Hertzog ]
  * d/control:
    - Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
    - Update team maintainer address to Debian Security Tools

  [ SZ Lin (林上智) ]
  * Add upstream metadata file
  * d/control:
    - Remove dh-autoreconf for Build-Depends: since it is enabled by default now
    - Bump debhelper version to 11
    - Bump Standards-Version to 4.2.1
  * d/copyright:
    - Replace "http" with "https"
    - Update author email
    - Update copyright info.
  * d/compat:
    - Bump compat version to 11

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 29 Aug 2018 13:51:39 +0800

undbx (0.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/copyright:
      - Added a new (extra) homepage.
      - Updated the upstream copyright years.
  * debian/patches/fix-makefile: updated.
  * debian/watch: added a new source.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 06 Jul 2015 09:44:35 -0300

undbx (0.20-2) unstable; urgency=medium

  * Team upload.
  * New upstream homepage. Thanks to Henri Salo <henri@nerv.fi>.
      (Closes: #787589)
  * Migrations:
      - DH level to 9.
      - Using dh-autoreconf now.
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Improved the long description.
      - Updated the Vcs-* fields.
  * debian/copyright:
      - Updated the Format field in header.
      - Updated all information.
  * debian/patches/fix-makefile: added to allow the GCC hardening. This will
      fix a possible FTBFS too. (Closes: #689702)
  * debian/rules: added the DEB_BUILD_MAINT_OPTIONS to improve the GCC
      hardening.
  * debian/undbx.1: fixed a typo. Thanks to A. Costa" <agcosta@gis.net>.
      (Closes: #695491).
  * debian/undbx.manpages: renamed to manpages.
  * debian/watch: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 17 Jun 2015 13:33:13 -0300

undbx (0.20-1) unstable; urgency=low

  * Team upload

  [ Christophe Monniez ]
  * Initial release. (Closes: #608114)

 -- Julien Valroff <julien@debian.org>  Sun, 05 Jun 2011 13:49:42 +0200
